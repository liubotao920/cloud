const router = require("koa-router")();
const util = require("./../util/util");

router.post("/debug/add", async (ctx, next) => {
  const { name, type, message, info, device, system, remark } = ctx.post;
  if (!info) {
    throw new Error("The Debug Info Can't Be Empty");
  }

  const debug = {
    baas_id: ctx.baas.id,
    name,
    type,
    message,
    info,
    device,
    system,
    remark
  };

  BaaS.redis.rpushAsync(
    util.getVersionKey("baas_log_debug"),
    JSON.stringify(debug)
  );

  ctx.success(debug, "success");
});

module.exports = router;
