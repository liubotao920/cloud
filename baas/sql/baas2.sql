-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `baas2` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `baas2`;

DROP TABLE IF EXISTS `auth`;
CREATE TABLE `auth` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `secret` text,
  `expires` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `baas_id` (`baas_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `baas`;
CREATE TABLE `baas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` text,
  `appid` text,
  `appkey` text,
  `database` text,
  `connection` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `private` int(11) NOT NULL DEFAULT '0' COMMENT '私有',
  `remark` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `appid` (`appid`(12)),
  KEY `appkey` (`appkey`(42))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `class`;
CREATE TABLE `class` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `baas_id` (`baas_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `class_auth`;
CREATE TABLE `class_auth` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `class_id` int(11) NOT NULL DEFAULT '0',
  `auth_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `baas_id` (`baas_id`),
  KEY `class_id` (`class_id`),
  KEY `auth_id` (`auth_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `class_field`;
CREATE TABLE `class_field` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `class_id` int(11) NOT NULL DEFAULT '0',
  `table` text,
  `name` text,
  `value` text,
  `type` text,
  `required` int(11) NOT NULL DEFAULT '0' COMMENT '客户端是否需要传值',
  `url` text COMMENT '过滤URL',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `baas_id` (`baas_id`),
  KEY `class_id` (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `class_function`;
CREATE TABLE `class_function` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `class_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `body` text,
  `remark` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `baas_id` (`baas_id`),
  KEY `class_id` (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `class_hook`;
CREATE TABLE `class_hook` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `class_id` int(11) NOT NULL DEFAULT '0',
  `table` text,
  `body` text,
  `remark` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `baas_id` (`baas_id`),
  KEY `class_id` (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `class_schedule`;
CREATE TABLE `class_schedule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `class_id` int(11) NOT NULL DEFAULT '0',
  `function_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `rule` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `baas_id` (`baas_id`),
  KEY `class_id` (`class_id`),
  KEY `function_id` (`function_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `class_table`;
CREATE TABLE `class_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `class_id` int(11) NOT NULL DEFAULT '0',
  `table` text,
  `fetch` int(11) NOT NULL DEFAULT '1',
  `add` int(11) NOT NULL DEFAULT '0',
  `update` int(11) NOT NULL DEFAULT '0',
  `delete` int(11) NOT NULL DEFAULT '0',
  `auth_id` int(11) NOT NULL DEFAULT '0' COMMENT '输出结果加密ID，0未加密',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `baas_id` (`baas_id`),
  KEY `class_id` (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `file`;
CREATE TABLE `file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `ext` text,
  `type` text,
  `size` int(11) NOT NULL DEFAULT '0',
  `savename` text,
  `savepath` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `baas_id` (`baas_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `global`;
CREATE TABLE `global` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `body` text,
  `remark` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `baas_id` (`baas_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `relation`;
CREATE TABLE `relation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `table` text,
  `target` text,
  `targeted` text COMMENT '关联昵称，默认target',
  `relation` text,
  `foreign_key` text,
  `foreign_key_target` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `baas_id` (`baas_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `table`;
CREATE TABLE `table` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `uuid` tinyint(1) NOT NULL DEFAULT '0' COMMENT '自增主键uuid',
  `hidden` text,
  `unique` text COMMENT '唯一值',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `baas_id` (`baas_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2018-02-03 05:58:54
