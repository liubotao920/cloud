const router = require("koa-router")();

/**
 *
 * api {get} /home/app/plugins/addons_poster 获取插件海报列表
 *
 */
router.get("/poster", async (ctx, nex) => {
  const baas = ctx.baas;
  const posters = await BaaS.Models.plugins_poster
    .forge()
    .fetchAll({ withRelated: ["image", "plugins"] });
  ctx.success(posters);
});

module.exports = router;
