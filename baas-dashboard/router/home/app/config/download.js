const router = require("koa-router")();
/**
 *
 * @api {get} /app/config/privilege/download 下载源码特权
 *
 */
router.get("/privilege/download", async (ctx, next) => {
  ctx.success("下载");
});

module.exports = router;
