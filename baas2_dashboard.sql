-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- 主机： db
-- 生成日期： 2020-03-10 09:49:09
-- 服务器版本： 5.7.21-log
-- PHP 版本： 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `baas2_dashboard`
--

--
-- 转存表中的数据 `charges`
--

INSERT INTO `charges` (`id`, `request_price`, `flow_price`, `storage_price`, `engine`, `storage`, `flow`, `request`, `charges`, `subtitle`, `type`, `created_at`, `updated_at`) VALUES
(1, 0.00, 0.000, 0.000, 0.00, 20.00, 2.00, 100000, 3.00, '中级用量配额，适用于创业项目、种子期项目', '个人版（3元/天）', NULL, NULL),
(2, 0.00, 0.000, 0.000, 0.00, 200.00, 20.00, 1000000, 30.00, '高级用量配额，适用于成熟项目', '企业版（30元/天）', NULL, NULL),
(3, 0.50, 0.300, 0.005, 2.00, 0.00, 0.00, 0, 0.00, '自由支配用量配额，灵活运用于所有项目', '标准版', NULL, NULL);

--
-- 转存表中的数据 `privilege`
--

INSERT INTO `privilege` (`id`, `name`, `price`, `remark`, `icon`, `detail`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '下载源码', 2999.00, '支持下载应用的源码', 'icon-download', '<div data-v-505ca098=\"\" style=\"margin-left: 10px;\"><p data-v-505ca098=\"\" class=\"privilege-des\">1、下载应用源码功能开通之后，永久有效。</p> <p data-v-505ca098=\"\" class=\"privilege-des\">2、下载的应用源码语言是Nodejs，请参考源码包安装方式安装。</p> <p data-v-505ca098=\"\" class=\"privilege-des\">3、下载的应用源码语言如需php，java，go等，请联系客服。</p> <p data-v-505ca098=\"\" class=\"privilege-des\">4、下载的应用源码支持独立部署，永久使用。</p></div>', NULL, NULL, NULL);

--
-- 转存表中的数据 `recharge`
--

INSERT INTO `recharge` (`id`, `price`, `discount_price`, `created_at`, `updated_at`) VALUES
(1, 100.00, 95.00, NULL, NULL),
(2, 500.00, 450.00, NULL, NULL),
(3, 1000.00, 850.00, NULL, NULL),
(4, 3000.00, 2400.00, NULL, NULL),
(5, 10000.00, 7500.00, NULL, NULL),
(6, 20000.00, 14000.00, NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
