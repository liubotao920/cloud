# 附近

------

#### 添加地点

|参数 | 必填 |  说明 |
| ---- | ------------------ | ---- |
| related_name | 是 | 经营资质主体 |
| related_credential | 是 | 经营资质证件号 |
| related_address | 是 | 经营资质地址 |
| related_proof_material | 是 | 相关证明材料照片临时素材mediaid |

```js
const addNearbyPoi = await wxa.addNearbyPoi({
    "related_name":"XXX公司", //经营资质主体
    "related_credential":"12345678-0", //经营资质证件号
    "related_address":"广州市新港中路397号TIT创意园", //经营资质地址
    "related_proof_material":"3LaLzqiTrQcD20DlX_o-OV1-nlYMu7sdVAL7SV2PrxVyjZFZZmB3O6LPGaYXlZWq" //相关证明材料照片临时素材mediaid
});
```

#### 删除地点

|参数 | 必填 |  说明 |
| ---- | ------------------ | ---- |
| poiId | 是 | 附近地点ID |

```js
const delNearbyPoi = await wxa.delNearbyPoi( poiId );
```

#### 展示/取消展示附近小程序

|参数 | 必填 |  说明 |
| ---- | ------------------ | ---- |
| poiId | 是 | 附近地点ID |
| status | 是 | 0：取消展示；1：展示 |

```js
const setNearbyPoiShowStatus = await wxa.setNearbyPoiShowStatus( poiId, status );
```
